from django.forms import Form, EmailField


class EmailForm(Form):
    email = EmailField(required=True)
