from django.apps import AppConfig


class CrmManagerConfig(AppConfig):
    name = 'crm_manager'
