import os
import redis
import datetime

from hashlib import sha256
from redis.exceptions import ConnectionError
from django.utils.crypto import get_random_string
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import View

from crm_manager.forms.EmailForm import EmailForm

PERIOD_OF_ACTIVITY = 600


class EmailView(View):

    def get(self, request):
        form = EmailForm()
        return render(request, 'crm_manager/login.html', {'form': form})

    def post(self, request):
        form = EmailForm(request.POST)
        if form.is_valid():
            user_email = form.cleaned_data['email']

            if check_user_existence(user_email):
                user_token = get_random_string(16)
                user_key = get_random_string(16)
                key_hash = sha256(user_key.encode()).hexdigest()
                expire_time = (datetime.datetime.now() + datetime.timedelta(0, 600)).strftime("%Y-%m-%d %H:%M:%S")

                login_url = "privacy.bemyapp.com/{}/{}".format(user_token, user_key)

                try:
                    r = redis.StrictRedis(host=os.environ['REDIS_HOST'],
                                          port=os.environ['REDIS_PORT'],
                                          db=os.environ['REDIS_DB'])
                    r.hmset(user_email,
                            {'token': user_token, 'key_hash': key_hash, 'expire_time': expire_time})

                except ConnectionError as e:
                    print(e)

                #return redirect(login_url)

        return HttpResponse('<h1>Hello</h1>')


class LoginView(View):

    def get(self, request):
        try:
            r = redis.StrictRedis(host=os.environ['REDIS_HOST'],
                                  port=os.environ['REDIS_PORT'],
                                  db=os.environ['REDIS_DB'])
            token, key_hash, expire_time = [field.decode("utf-8")
                                            for field in r.hmget('email', ['token', 'key_hash', 'expire_time'])]
            expire_time = datetime.datetime.strptime(expire_time, '%Y-%m-%d %H:%M:%S.%K')

            if expire_time > datetime.datetime.now():
                # send email
                pass
            else:
                # expired, no access to data
                pass

        except ConnectionError:
            return redirect(request, '/crm_manager/success/')


def check_user_existence(user_email):
    # request to api
    return True

