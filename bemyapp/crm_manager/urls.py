from django.urls import path

from . import views

urlpatterns = [
    path('login/', views.EmailView.as_view(), name='login'),
]